$(function(){
    $("#header").load("header.html");
    $("#footer").load("footer.html"); //TODO: atm empty
});

function openSidebar() {
    document.getElementById("mySidebar").style.left = "0";
    showCloseArea();
}

function closeSidebar() {
    document.getElementById("mySidebar").style.left = "-60vw";
    hideCloseArea();
}

function showCloseArea() {
    //document.getElementById("closingArea").style.left = "0";
    var closingArea = document.getElementById("closingArea");
    closingArea.style.visibility = 'visible';
    closingArea.style.backgroundColor = 'rgba(0, 0, 0, 0.5)';

}

function hideCloseArea() {
    // document.getElementById("closingArea").style.left = "-100vw";
    var closingArea = document.getElementById("closingArea");
    closingArea.style.visibility = 'hidden';
    closingArea.style.backgroundColor = 'rgba(0, 0, 0, 0)';
}
function showMessage() {
    var message = document.getElementById("safe-message-container");
    message.style.display="block";
    setTimeout(function () {
        message.style.display="none"
    },2000)
}
